package edu.olehhalila.mq.sdk.api.models.message;

public enum MessageAcknowledgement {
    NONE,
    LEADER_ONLY,
    ALL
}
