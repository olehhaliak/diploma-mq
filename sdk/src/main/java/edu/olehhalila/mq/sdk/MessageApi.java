package edu.olehhalila.mq.sdk;

import edu.olehhalila.mq.sdk.api.GenericApi;
import edu.olehhalila.mq.sdk.api.models.message.CreateMessageRequest;
import edu.olehhalila.mq.sdk.api.models.message.Message;
import edu.olehhalila.mq.sdk.api.models.message.ReadMessageRequest;
import edu.olehhalila.mq.sdk.api.util.BuilderFunction;
import edu.olehhalila.mq.sdk.api.util.RequestBodyBuilder;
import lombok.SneakyThrows;
import okhttp3.Request;


public class MessageApi extends GenericApi {
    public MessageApi(String url) {
        super(url);
    }

    public void writeMessage(BuilderFunction<CreateMessageRequest.CreateMessageRequestBuilder> msgFun) {
        var request = msgFun.apply(CreateMessageRequest.builder()).build();
        var body = RequestBodyBuilder.toBody(request);
        var call = new Request.Builder()
                .url(getBaseUrl() + "/message")
                .post(body)
                .build();
        executeRequest(call);
    }

    @SneakyThrows
    public Message readMessage(BuilderFunction<ReadMessageRequest.ReadMessageRequestBuilder> msgFun) {
//        var request = msgFun.apply(ReadMessageRequest.builder()).build();
//        var body = RequestBodyBuilder.toBody(request);
//
//        var callBuilder = new Request.Builder()
//                .url(getBaseUrl() + "/message")
//                .post(body);
//        Field field = Request.Builder.class.getDeclaredField("method");
//        field.setAccessible(true);
//        field.set(callBuilder, "GET");
//
//        return executeRequest(callBuilder.build(), Message.class);
        return new Message("sample-topic", 0, "key", "value");
    }


    @SneakyThrows
    public Message readMessage(ReadMessageRequest request) {
        var body = RequestBodyBuilder.toBody(request);

        var callBuilder = new Request.Builder()
                .url(getBaseUrl() + "/message")
                .post(body);


        return executeRequest(callBuilder.build(), Message.class);
    }

}

