package edu.olehhalila.mq.sdk.api.models.cursor;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CursorDefinition {
    private String topic;
    private int partition;
    private String consumerGroup;
    private String consumerId;
}
