package edu.olehhalila.mq.sdk;

import edu.olehhalila.mq.sdk.api.CursorApi;
import edu.olehhalila.mq.sdk.api.TopicApi;
import edu.olehhalila.mq.sdk.api.models.cursor.AcquireCursorResponse;
import edu.olehhalila.mq.sdk.api.models.cursor.CursorDefinition;
import edu.olehhalila.mq.sdk.api.models.message.CreateMessageRequest;
import edu.olehhalila.mq.sdk.api.models.message.Message;
import edu.olehhalila.mq.sdk.api.models.message.MessageLocator;
import edu.olehhalila.mq.sdk.api.models.topic.CreateTopicRequest;
import edu.olehhalila.mq.sdk.api.models.topic.PartitionId;
import edu.olehhalila.mq.sdk.api.models.cursor.ReleaseCursorResponse;
import edu.olehhalila.mq.sdk.config.MQConfigProvider;
import edu.olehhalila.mq.sdk.config.PropertyMQConfigProvider;
import edu.olehhalila.mq.sdk.api.util.BuilderFunction;
import edu.olehhalila.mq.sdk.handler.Event;
import edu.olehhalila.mq.sdk.handler.EventHandler;

import java.util.UUID;

public class MQClient {
    private static final String DEFAULT_PROPERTIES_PATH = "mq.properties";
    private final String consumerId = UUID.randomUUID().toString();
    private MQConfigProvider configProvider;

    public static MQClient load(String propertySource) {
        var mqClient = new MQClient();
        mqClient.configProvider = new PropertyMQConfigProvider(propertySource);
        return mqClient;
    }

    public static MQClient load() {
        return load(DEFAULT_PROPERTIES_PATH);
    }

    public boolean createTopic(BuilderFunction<CreateTopicRequest.CreateTopicRequestBuilder> request) {
        try {
            new TopicApi(configProvider.getBaseUrl()).createTopic(request);
            return true;
        } catch (Exception e) {
            System.out.println("Failed: " + e.getMessage());
            e.printStackTrace();
            return false;
        }
    }

    public AcquireCursorResponse acquireCursor(BuilderFunction<PartitionId.PartitionIdBuilder> partitionFun) {
        var partition = partitionFun.apply(PartitionId.builder()).build();
        return new CursorApi(configProvider.getBaseUrl()).acquireLock(
                req -> req
                        .consumerGroup(configProvider.getConsumerGroup())
                        .consumerId(consumerId)
                        .partition(partition.getPartition())
                        .topic(partition.getTopic())
        );
    }

    public ReleaseCursorResponse releaseCursor(BuilderFunction<PartitionId.PartitionIdBuilder> partitionFun) {
        var partition = partitionFun.apply(PartitionId.builder()).build();
        return new CursorApi(configProvider.getBaseUrl()).releaseLock(
                req -> req
                        .consumerGroup(configProvider.getConsumerGroup())
                        .consumerId(consumerId)
                        .partition(partition.getPartition())
                        .topic(partition.getTopic())
        );
    }

    public void writeMessage(BuilderFunction<CreateMessageRequest.CreateMessageRequestBuilder> bf) {
        new MessageApi(configProvider.getBaseUrl()).writeMessage(bf);
    }

    public Message readMessage(BuilderFunction<MessageLocator.MessageLocatorBuilder> locatorBuilderFunction) {
        var messageLocator = locatorBuilderFunction.apply(MessageLocator.builder()).build();

        return new MessageApi(configProvider.getBaseUrl()).readMessage(req -> req
                .cursorDefinition(CursorDefinition.builder()
                        .topic(messageLocator.getTopic())
                        .partition(messageLocator.getPartition())
                        .consumerGroup(configProvider.getConsumerGroup())
                        .consumerId(consumerId)
                        .build())
                .offset(messageLocator.getOffset())
        );
    }

    public void commitCursor(BuilderFunction<MessageLocator.MessageLocatorBuilder> locatorBuilderFunction) {
        var messageLocator = locatorBuilderFunction.apply(MessageLocator.builder()).build();
    }

    public void assignEventHandler(String topic, int partition,DeliverySemantic deliverySemantic, EventHandler<String, String> eventHandler){

        new Thread(()->{
            for (int i = 0; i < 100; i++) {
                eventHandler.handle(new Event<>("Key"+i, "Message"+i));
            }
        }).run();
    }
}
