package edu.olehhalila.mq.sdk.api;

import edu.olehhalila.mq.sdk.api.models.topic.CreateTopicRequest;
import edu.olehhalila.mq.sdk.api.util.BuilderFunction;
import edu.olehhalila.mq.sdk.api.util.RequestBodyBuilder;
import okhttp3.*;

public class TopicApi extends GenericApi {

    public TopicApi(String url) {
        super(url);
    }

    public void createTopic(BuilderFunction<CreateTopicRequest.CreateTopicRequestBuilder> createTopicRequest) {
        var request = createTopicRequest.apply(CreateTopicRequest.builder()).build();
        var body = RequestBodyBuilder.toBody(request);
        var call = new Request.Builder()
                .url(getBaseUrl() + "/topic")
                .post(body)
                .build();
        executeRequest(call);
    }

}
