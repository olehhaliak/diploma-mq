package edu.olehhalila.mq.sdk.api.models.message;

import edu.olehhalila.mq.sdk.api.models.cursor.CursorDefinition;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ReadMessageRequest {
    private CursorDefinition cursorDefinition;
    private int offset;

}
