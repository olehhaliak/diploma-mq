package edu.olehhalila.mq.sdk.api.models.cursor;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ReleaseCursorResponse {
    private boolean success;
    private String err;

}
