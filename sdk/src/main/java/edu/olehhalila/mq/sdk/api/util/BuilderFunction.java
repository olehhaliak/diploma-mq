package edu.olehhalila.mq.sdk.api.util;

import java.util.function.Function;

public interface BuilderFunction<B> extends Function<B, B> {
}
