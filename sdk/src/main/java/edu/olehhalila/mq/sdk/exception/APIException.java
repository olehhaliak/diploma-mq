package edu.olehhalila.mq.sdk.exception;

public class APIException extends RuntimeException{

    public APIException(String message) {
        super(message);
    }
}
