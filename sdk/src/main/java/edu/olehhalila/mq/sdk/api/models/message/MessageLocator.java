package edu.olehhalila.mq.sdk.api.models.message;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class MessageLocator {
    private String topic;
    private int partition;
    private int offset;

}
