package edu.olehhalila.mq.sdk.api.models.topic;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CreateTopicRequest {
    private String name;
    private int partitions;
    private int replicationFactor = 1;
}
