package edu.olehhalila.mq.sdk.api;

import edu.olehhalila.mq.sdk.exception.APIException;
import edu.olehhalila.mq.sdk.api.models.cursor.AcquireCursorResponse;
import edu.olehhalila.mq.sdk.api.models.cursor.CursorDefinition;
import edu.olehhalila.mq.sdk.api.models.cursor.ReleaseCursorResponse;
import edu.olehhalila.mq.sdk.api.util.BuilderFunction;
import edu.olehhalila.mq.sdk.api.util.RequestBodyBuilder;
import okhttp3.Request;

public class CursorApi extends GenericApi {
    public CursorApi(String url) {
        super(url);
    }

    public AcquireCursorResponse acquireLock(BuilderFunction<CursorDefinition.CursorDefinitionBuilder> cursorDef) {
        var request = cursorDef.apply(CursorDefinition.builder()).build();
        var body = RequestBodyBuilder.toBody(request);
        var call = new Request.Builder()
                .url(getBaseUrl() + "/consumer/cursor/acquire")
                .post(body)
                .build();
        var response = executeRequest(call, AcquireCursorResponse.class);
        if(response.getErrCode()!= null) {
            throw new APIException(response.getErrCode());
        }
        return response;
    }

    public ReleaseCursorResponse releaseLock(BuilderFunction<CursorDefinition.CursorDefinitionBuilder> cursorDef) {
        var request = cursorDef.apply(CursorDefinition.builder()).build();
        var body = RequestBodyBuilder.toBody(request);
        var call = new Request.Builder()
                .url(getBaseUrl() + "/consumer/cursor/release")
                .post(body)
                .build();
        var response = executeRequest(call, ReleaseCursorResponse.class);

        if(response.getErr()!= null) {
            throw new APIException(response.getErr());
        }
        return response;
    }
}
