package edu.olehhalila.mq.sdk;


import edu.olehhalila.mq.sdk.api.models.message.Message;
import edu.olehhalila.mq.sdk.handler.Event;

import static edu.olehhalila.mq.sdk.DeliverySemantic.AT_LEAST_ONCE;

public class Main {
    public static void main(String... args) {
        var mqClient = MQClient.load();
        mqClient.assignEventHandler("sampel-topic", 0, AT_LEAST_ONCE, Main::handleMessage);
    }

    public static void handleMessage(Event<String, String> event) {
        System.out.println("Message received: " + event.getKey() + " - " + event.getValue());
    }


//
//        mqClient.acquireCursor(req -> req
//                .topic("the-topic")
//                .partition(0)
//        );
//
//        mqClient.commitCursor(req -> req
//                .topic("the-topic")
//                .partition(0)
//                .offset(12)
//        );
//
//        mqClient.releaseCursor(req -> req
//                .topic("the-topic")
//                .partition(0)
//        );
//
//        var msg = mqClient.readMessage(req -> req
//                .topic("the-topic")
//                .partition(0)
//                .offset(0)
//        );
//        System.out.println(msg);
}


//
//        client.createTopic(req -> req
//                .name("sdk-2")
//                .partitions(1)
//                .replicationFactor(1)
//        );
//        client.writeMessage(req->req
//                .topic("sdk-2")
//                .partition(0)
//                .key("hello")
//                .value("world")
//        );
//        var acquireCursorResponse = client.acquireCursor(req -> req
//                .partition(0)
//                .topic("sdk-2")
//        );
//        var msg = client.readMessage(req -> req
//                .topic("sdk-2")
//                .partition(0)
//                .offset(0)
//        );
//        System.out.println(msg);
//        System.out.println(acquireCursorResponse);
//        var releaseCursorResponse = client.releaseCursor(req -> req
//                .partition(0)
//                .topic("sdk-2")
//        );

//        client.assignEventHandler("sampel-topic",0,event->
//
//    {
//        System.out.println("Message received: " + event.getKey() + " - " + event.getValue());
//    });
//
//
//}
//}