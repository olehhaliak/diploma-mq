package edu.olehhalila.mq.sdk.api.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import okhttp3.MediaType;
import okhttp3.RequestBody;

public class RequestBodyBuilder {

    private static final ObjectMapper objectMapper = new ObjectMapper();

    @SneakyThrows
    public static  <T> RequestBody toBody(T payload) {
        var json = objectMapper.writeValueAsString(payload);

        return RequestBody.create(json, MediaType.get("application/json; charset=utf-8"));


    }
}
