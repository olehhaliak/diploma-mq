package edu.olehhalila.mq.sdk.api.models.topic;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PartitionId {
    private String topic;
    private int partition;
}
