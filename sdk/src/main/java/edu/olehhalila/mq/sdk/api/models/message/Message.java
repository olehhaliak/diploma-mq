package edu.olehhalila.mq.sdk.api.models.message;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Message {
    private String topic;
    private int partition;
    private String key;
    private String value;
}
