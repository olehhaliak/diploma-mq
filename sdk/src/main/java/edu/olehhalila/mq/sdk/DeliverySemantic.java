package edu.olehhalila.mq.sdk;

public enum DeliverySemantic {
    AT_MOST_ONCE,
    AT_LEAST_ONCE
}
