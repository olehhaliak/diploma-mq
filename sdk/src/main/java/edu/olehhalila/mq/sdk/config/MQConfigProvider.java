package edu.olehhalila.mq.sdk.config;

public interface MQConfigProvider {

    String getBaseUrl();
    String getConsumerGroup() ;

}
