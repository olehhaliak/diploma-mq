package edu.olehhalila.mq.sdk.config;

import lombok.SneakyThrows;

import javax.swing.*;
import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

public class PropertyMQConfigProvider implements MQConfigProvider {

    private static final String BASE_URL_PROPERTY = "bootstrap.server.url";
    private static final String CONSUMER_GROUP = "consumer.group";
    private final Properties properties;

    @SneakyThrows
    public PropertyMQConfigProvider(String propertySource) {
        var propertyFile = new File(propertySource);
        if (!propertyFile.exists() || propertyFile.isDirectory()) {
            throw new IllegalArgumentException("property file specified not found");
        }
        properties = new Properties();
        try (var fis = new FileInputStream(propertyFile)) {
            properties.load(fis);
        }
    }

    @Override
    public String getBaseUrl() {
        return properties.getProperty(BASE_URL_PROPERTY);
    }

    @Override
    public String getConsumerGroup() {
        return properties.getProperty(CONSUMER_GROUP);
    }
}
