package edu.olehhalila.mq.sdk.handler;

public interface EventHandler<K, V> {

    void handle( Event<K, V> message);

}
