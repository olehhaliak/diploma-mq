package edu.olehhalila.mq.sdk.handler;

import lombok.Data;

@Data
public class Event<K, V> {
    K key;
    V value;

    public Event(K key, V value) {
        this.key = key;
        this.value = value;
    }
}
