package edu.olehhalila.mq.sdk.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

import java.io.IOException;

@RequiredArgsConstructor
public class GenericApi {
    private static final OkHttpClient client = new OkHttpClient();
    private static ObjectMapper mapper = new ObjectMapper();
    private final String url;

    @SneakyThrows
    protected Response executeRequest(Request request) {
        try (Response response = client.newCall(request).execute()) {
            if (!response.isSuccessful()) {
                throw new IOException("Unexpected code " + response);
            }
            return response;
        }
    }

    @SneakyThrows
    protected <T> T executeRequest(Request request, Class<T> tClass) {
        try (Response response = client.newCall(request).execute()) {
            if (!response.isSuccessful()) {
                throw new IOException("Unexpected code " + response);
            }
            return resolveResponse(response, tClass);
        }
    }

    protected String getBaseUrl() {
        return url;
    }

    @SneakyThrows
    public <T> T resolveResponse(Response response, Class<T> tClass) {
        if (response.body() == null) {
            return null;
        }
        return mapper.readValue(response.body().bytes(), tClass);
    }
}
