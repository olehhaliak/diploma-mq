Diploma MQ
---
🤖 :  
*-* Hello, fellow human being! I've never seen you before, let me show you around here  

⚠️ This particular version of code contains some config that were added after the report submission to anti-plagiarism :
  - Added easy-to-follow instruction, endpoint to explore an API, and proper docker support
  - ❗ ️All the config changes here were made ***before*** the anti-plagiarism results were published
  - ❗ Not a single piece of actual business logic has been altered. Not a single code snippet mentioned in the report has been changed.  

If you really need the version, that was submitted before Jun the 5th - you will find it [under this tag](https://gitlab.com/olehhaliak/diploma-mq/-/tree/jun-the-4th?ref_type=tags)

🤖 :  
*-* Enough precautions for today  
*-* So, the project is split into two main parts: MQ server and SDK

---
### MQ server
🤖 :  
*-* First of all you have to know that the server can be run in a standalone mode(single instance), or as a cluster. We gonna build the project first [optional], and then I'll shouw you how to use both

#### Build 🏗

🤖 :  
*-* If you gonna use docker(and this is the suggested way) to run the thingy - you can skip this step.
- You'll need JDK 21+ adn Maven 3.9+
- Run `.mvnv clean install`
- Ready-to-execute jar will be located within `target` directory
- Use regular `java -jar` command to run it

---
#### Run 🚀
🤖 :  
*-* Let's start this bird up now!  
###### Standalone
🤖 :  
*-* If you need it for local development - ***standalone mode*** is for you  
*-* To run the MQ Server in a standalone mode just execute
```shell
docker run --rm -it -p 9090:9090 justflick/oh-diploma-mq
```

###### Cluster
🤖 :  
*-* One MQ server is indeed cool. But you know what is even cooler? - Having three MQ servers running in a single cluster altogether!  
*-* Luckily for you, my flesh and bones friend, you can do just that in a single command by executing
```shell
docker compose up
```
*-* enjoy

---
#### Explore the API 🗺
🤖 :  
*-* There are plenty of handy endpoints you can interact with on the server  
*-* You can learn all about them on the dedicated OpenAPI page by URL:
``` 
http://localhost:9090/swagger-ui/index.html
```

---
### SDK
🤖 :  
*-* As of now, our SDK is passing mvnrepository artifactory checks, so you have to wait until then if you want to connect it as a dependency to your project right not  
*-* You can still find all the sources under the the `./sdk` folder, which you can pass manually to you project


