package edu.olehhaliak.mq.topology;

import edu.olehhaliak.mq.topic.model.Partition;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/topology")
@RequiredArgsConstructor
public class TopologyController {
    private final PartitionDistributionService partitionDistributionService;

    @GetMapping
    public Map<Partition,String> getTopology(){
        return partitionDistributionService.getPartitionDistribution();
    }

    @PutMapping
    public void updateTopology(@RequestBody Map<String, BrokerDistribution> partitionDist){
        partitionDistributionService.setBrokerDistMap(partitionDist);
    }
}
