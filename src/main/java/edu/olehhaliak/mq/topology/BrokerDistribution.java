package edu.olehhaliak.mq.topology;

import edu.olehhaliak.mq.topic.model.Partition;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static java.util.function.Predicate.not;

@Data
@ToString
@NoArgsConstructor
public class BrokerDistribution {
    private String brokerId;
    private List<Partition> partitions = new ArrayList<>();

    public BrokerDistribution(String brokerId) {
        this.brokerId = brokerId;
    }

    public int getLeadersCount(){
        return (int) partitions.stream()
                .filter(Partition::isLeader)
                .count();
    }

    public int getReplicaCount(){
        return (int) partitions.stream()
                .filter(not(Partition::isLeader))
                .count();
    }
}
