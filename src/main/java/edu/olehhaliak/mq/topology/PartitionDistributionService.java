package edu.olehhaliak.mq.topology;

import com.fasterxml.jackson.core.type.TypeReference;
import edu.olehhaliak.mq.config.BrokerConfig;
import edu.olehhaliak.mq.config.util.VersionedGenericConfigPersister;
import edu.olehhaliak.mq.discoverability.registry.BrokerRegistryService;
import edu.olehhaliak.mq.topic.model.Partition;
import edu.olehhaliak.mq.topic.model.Topic;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Stream;

import static java.util.Comparator.comparing;
import static java.util.function.Predicate.not;
import static java.util.stream.Collectors.groupingBy;

@Slf4j
@Service
@RequiredArgsConstructor
public class PartitionDistributionService {//TODO add rebalance
    private static final String TOPOLOGY_CONFIG = "topology";
    private final BrokerRegistryService brokerRegistryService;
    private final VersionedGenericConfigPersister configPersister;
    private final BrokerConfig brokerConfig;
    Map<String, BrokerDistribution> brokerDistributionMap = new HashMap<>();


    @PostConstruct
    public void init(){
        brokerDistributionMap = configPersister.loadLatestConfig(TOPOLOGY_CONFIG,new TypeReference<Map<String, BrokerDistribution>>(){})
                        .orElse(new ConcurrentHashMap<>());
        if(brokerDistributionMap.isEmpty()){
            brokerRegistryUpdated();
        }
    }

    /**
     * @return map of Partition and broker where it is hosted
     */
    public Map<Partition, String> getPartitionDistribution(){
            var partitionDistribution = new HashMap<Partition, String>();
            brokerDistributionMap.forEach((brokerId, dist) -> {
                dist.getPartitions().forEach(partition -> partitionDistribution.put(partition, brokerId));
            });
            return partitionDistribution;
    }

    public Map<String, BrokerDistribution> getBrokerDistributionMap(){
        return brokerDistributionMap;
    }

    public BrokerDistribution getCurrentBrokerDistribution(){
        return brokerDistributionMap.get(brokerConfig.getBrokerId());
    }

    public void setBrokerDistMap(Map<String,BrokerDistribution> brokerDistributionMap){
        this.brokerDistributionMap = brokerDistributionMap;
        configPersister.persistConfig(TOPOLOGY_CONFIG, brokerDistributionMap);
    }

    public synchronized void distributeNewTopic(Topic topic) {
        if (topic.getParams().getReplicationFactor() > brokerDistributionMap.size()) {
            throw new IllegalStateException("Not enough brokers to ensure replication");
        }
        distributeAllTopicPartitions(topic.getPartitions());
        brokerDistributionMap.values().forEach(System.out::println);
        configPersister.persistConfig(TOPOLOGY_CONFIG,brokerDistributionMap);
    }

    public boolean isLeader(String topic, int partitionNum) {
        return getCurrentBrokerRelatedPartitions(topic, partitionNum)
                .filter(Partition::isLeader)
                .count() == 1;
    }

    public boolean isReplica(String topic, int partitionNum) {
        return getCurrentBrokerRelatedPartitions(topic, partitionNum)
                .filter(not(Partition::isLeader))
                .count() == 1;
    }

    private Stream<Partition> getCurrentBrokerRelatedPartitions(String topic, int partitionNum) {
        var dist = getCurrentBrokerDistribution();
        return dist.getPartitions()
                .stream()
                .filter(partition -> partition.getTopicName().equals(topic))
                .filter(partition -> partition.getPartitionNumber() == partitionNum);
    }

    private void distributeAllTopicPartitions(Collection<Partition> topicPartitions){
       topicPartitions.stream()
               .collect(groupingBy(Partition::getPartitionNumber))
               .values()
               .forEach(this::distributeNewPartition);
    }
    private void distributeNewPartition(List<Partition> partitionWithReplicas) {
        partitionWithReplicas.sort(comparing(Partition::isLeader).reversed());
        brokerDistributionMap.values().stream()
                .sorted(comparing(BrokerDistribution::getLeadersCount)
                        .thenComparing(BrokerDistribution::getReplicaCount)
                )
                .limit(partitionWithReplicas.size())
                .forEach(bd -> {
                    bd.getPartitions().add(partitionWithReplicas.get(0));
                    partitionWithReplicas.remove(0);
                });
    }

    public void brokerRegistryUpdated() {
        log.info("Updating topics distribution");
        var brokerRegistry = brokerRegistryService.getRegistry();

        var registryBrokerIds = brokerRegistry.getBrokers().keySet();
        var knownBrokerIds = brokerDistributionMap.keySet();

        var addedBrokerIds = registryBrokerIds.stream()
                .filter(id -> !knownBrokerIds.contains(id))
                .toList();

        var removedBrokerIds = knownBrokerIds.stream()
                .filter(id -> !registryBrokerIds.contains(id))
                .toList();

        addedBrokerIds.forEach(this::addNewBrokerDistribution);

        var lostPartitions = removedBrokerIds.stream()
                .map(brokerDistributionMap::get)
                .map(BrokerDistribution::getPartitions)
                .flatMap(List::stream)
                .toList();

        removedBrokerIds.forEach(brokerDistributionMap::remove);

        lostPartitions.stream()
                .collect(groupingBy(Partition::getTopicName))
                .values()
                .forEach(this::distributeAllTopicPartitions);
        log.info("Redistribution complete");
        configPersister.persistConfig(TOPOLOGY_CONFIG, brokerDistributionMap);
        brokerDistributionMap.values().forEach(System.out::println);
    }

    private void addNewBrokerDistribution(String brokerId){
        brokerDistributionMap.put(brokerId, new BrokerDistribution(brokerId));
    }
}
