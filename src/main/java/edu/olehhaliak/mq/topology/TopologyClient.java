package edu.olehhaliak.mq.topology;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.Map;

@Component
@RequiredArgsConstructor
public class TopologyClient {
    private final RestTemplate restTemplate;

    public void updateTopology(String brokerUrl, Map<String, BrokerDistribution> dist) {
        var url = brokerUrl + "/topology";
        restTemplate.put(url, dist);
    }
}
