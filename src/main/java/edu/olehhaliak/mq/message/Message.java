package edu.olehhaliak.mq.message;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Message {
    private String topic;
    private int partition;
    private String key;
    private String value;
    private MessageAcknowledgement ack = MessageAcknowledgement.ALL;
}
