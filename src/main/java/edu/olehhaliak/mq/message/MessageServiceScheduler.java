package edu.olehhaliak.mq.message;

import lombok.AllArgsConstructor;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

/**
 * Allows async execution of some MesageService methods
 */
@Component
public class MessageServiceScheduler {

    @Async
    public void runAsync(Runnable runnable){
        runnable.run();
    }
}
