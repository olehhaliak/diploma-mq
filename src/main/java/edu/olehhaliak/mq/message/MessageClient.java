package edu.olehhaliak.mq.message;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
@RequiredArgsConstructor
public class MessageClient {

    private final RestTemplate restTemplate;

    public void saveMessage(String leaderUrl, Message message){
        var url = leaderUrl + "/message";
        restTemplate.postForEntity(url,message,Void.class);
    }

    public void replicateMessage(String leaderUrl, Message message){
        var url = leaderUrl + "/message/replicate";
        restTemplate.postForEntity(url,message,Void.class);
    }

}
