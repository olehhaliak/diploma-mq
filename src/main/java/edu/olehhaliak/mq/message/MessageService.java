package edu.olehhaliak.mq.message;

import edu.olehhaliak.mq.config.BrokerConfig;
import edu.olehhaliak.mq.cursor.CursorService;
import edu.olehhaliak.mq.discoverability.registry.BrokerRegistryService;
import edu.olehhaliak.mq.persistance.IndexManager;
import edu.olehhaliak.mq.persistance.IndexManagerImpl;
import edu.olehhaliak.mq.persistance.log.Log;
import edu.olehhaliak.mq.topic.model.Partition;
import edu.olehhaliak.mq.topology.PartitionDistributionService;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.stream.Stream;

import static edu.olehhaliak.mq.message.MessageAcknowledgement.*;
import static java.util.function.Predicate.not;

@Service
@RequiredArgsConstructor
@Slf4j
public class MessageService {
    private final PartitionDistributionService partitionDistributionService;
    private final BrokerRegistryService brokerRegistryService;
    private final BrokerConfig brokerConfig;
    private final MessageClient messageClient;
    private final MessageServiceScheduler messageServiceScheduler;
    private final CursorService cursorService;
    private IndexManager indexManager;

    @PostConstruct
    protected void init() {
        indexManager = new IndexManagerImpl(brokerConfig.getRootDir() + "/log");
    }

    public Message readMessage(ReadMessageRequest request) {
        if (!cursorService.isCursorHolder(request.getCursorDefinition())) {//TODO: also check for leadership
            return null;
        }
        var log = indexManager.readLog(request.getCursorDefinition().toPartitionId(), request.getOffset());
        return Message.builder()
                .key(log.getKey())
                .value(log.getValue())
                .topic(log.getTopic())
                .partition(log.getPartition())
                .build();
    }

    /**
     * leaderSave
     *
     * @param message
     */
    public void saveMessage(Message message) {
        if (!partitionDistributionService.isLeader(message.getTopic(), message.getPartition())) {
            rerouteToLeader(message);
            return;
        }
        if (message.getAck() == NONE) {
            messageServiceScheduler.runAsync(() -> leaderSave(message));
        } else {
            leaderSave(message);
        }
    }

    protected void leaderSave(Message message) {
        log.debug("Save message on leader {}", message);
        saveMessageToLocalIndex(message);
        if (message.getAck() == ALL) {
            propagateToReplicas(message);
        } else {
            messageServiceScheduler.runAsync(() -> propagateToReplicas(message));
        }
    }

    private void propagateToReplicas(Message message) {
        getRelatedPartitions(message.getTopic(), message.getPartition())
                .filter(not(Partition::isLeader))
                .map(this::getPartitionBrokerUrl)
                .forEach(url -> messageClient.replicateMessage(url, message));
    }

    private void rerouteToLeader(Message message) {
        log.debug("Rerouting message to leader {}", message);
        getRelatedPartitions(message.getTopic(), message.getPartition())
                .filter(Partition::isLeader)
                .map(this::getPartitionBrokerUrl)
                .forEach(url -> messageClient.saveMessage(url, message));
    }

    public void replicateMessage(Message message) {
        if (partitionDistributionService.isReplica(message.getTopic(), message.getPartition())) {
            log.debug("Replicating message {}", message);
            saveMessageToLocalIndex(message);
        }
    }


    public void saveMessageToLocalIndex(Message message) {
        var logToSave = Log.builder()
                .topic(message.getTopic())
                .partition(message.getPartition())
                .key(message.getKey())
                .value(message.getValue())
                .build();
        indexManager.append(logToSave);
    }


    private Stream<Partition> getRelatedPartitions(String topic, int partitionNum) {
        return partitionDistributionService.getPartitionDistribution().keySet().stream()
                .filter(partition -> partition.getTopicName().equals(topic))
                .filter(partition -> partition.getPartitionNumber() == partitionNum);
    }

    private String getPartitionBrokerUrl(Partition partition) {
        var brokerId = partitionDistributionService.getPartitionDistribution().get(partition);
        return brokerRegistryService.getRegistry().getBrokers().get(brokerId);
    }
}
