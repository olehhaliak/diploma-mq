package edu.olehhaliak.mq.message;

public enum MessageAcknowledgement {
    NONE,
    LEADER_ONLY,
    ALL
}
