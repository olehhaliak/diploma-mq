package edu.olehhaliak.mq.message.api;

import edu.olehhaliak.mq.message.Message;
import edu.olehhaliak.mq.message.MessageService;
import edu.olehhaliak.mq.message.ReadMessageRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("message")
@RequiredArgsConstructor
public class MessageController {
    private final MessageService messageService;

    @PostMapping
    public void writeMessage(@RequestBody Message message) {
        messageService.saveMessage(message);
    }

    @PostMapping("/replicate")
    public void replicateMessage(@RequestBody Message message) {
        messageService.replicateMessage(message);

    }

    @GetMapping
    public Message readMessage(@RequestBody ReadMessageRequest request){
        return messageService.readMessage(request);
    }

}
