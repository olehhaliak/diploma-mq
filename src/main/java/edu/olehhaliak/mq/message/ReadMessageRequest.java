package edu.olehhaliak.mq.message;

import edu.olehhaliak.mq.cursor.CursorDefinition;
import lombok.Data;

@Data
public class ReadMessageRequest {
    private CursorDefinition cursorDefinition;
    private int offset;

}
