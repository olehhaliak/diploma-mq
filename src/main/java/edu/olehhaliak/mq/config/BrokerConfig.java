package edu.olehhaliak.mq.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.annotation.PostConstruct;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.PostMapping;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

@Data
@Configuration
@RequiredArgsConstructor
public class BrokerConfig {
    private final ObjectMapper objectMapper;

    String brokerId; ;
    @Value("${broker.url}")
    String url;
    @Value("${broker.follower.bootstrap-server.url:none}")
    String bootrapServerUrl;
    @Value("${broker.standalone-mode:false}")
    boolean isStandalone;
    @Value("${broker.root-dir:root}")
    String rootDir;

    @PostConstruct
    public void init() throws IOException {

        var configFile = new File(rootDir+"/config/broker.json");
        if(configFile.exists()){
           var persistableConfig = objectMapper.readValue(configFile, BrokerPersistableConfig.class);
           brokerId = persistableConfig.getBrokerId();
        }else {
            brokerId= UUID.randomUUID().toString();
            var persistableConfigation = new BrokerPersistableConfig();
            persistableConfigation.setBrokerId(brokerId);
            configFile.getParentFile().mkdirs();
            objectMapper.writeValue(configFile,persistableConfigation);
        }
    }
}
