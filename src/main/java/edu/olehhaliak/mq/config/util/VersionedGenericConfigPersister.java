package edu.olehhaliak.mq.config.util;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import edu.olehhaliak.mq.config.BrokerConfig;
import edu.olehhaliak.mq.topic.model.Topic;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Map;
import java.util.Optional;

@Component
@RequiredArgsConstructor
@Slf4j
public class VersionedGenericConfigPersister {
    private final BrokerConfig brokerConfig;
    private final ObjectMapper objectMapper;

    /**
     * persist config
     *
     * @return last version id
     */
    public <T> long persistConfig(String configName, T config) {
        var currentTime = System.currentTimeMillis();

        var newVersionFile = new File(getConfigDir(configName), currentTime + ".json");
        try {
            objectMapper.writeValue(newVersionFile, config);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return currentTime;
    }

    public <T> Optional<T> loadLatestConfig(String configName, TypeReference<T> type) {
        return Arrays.stream(getConfigDir(configName).list())
                .max(this::compareVersions)
                .map(latestVersion -> loadConfig(configName, latestVersion, type));
    }

    public long getLatestVersion(String configName){
        return Arrays.stream(getConfigDir(configName).list())
                .max(this::compareVersions)
                .map(latestVersionFile ->  Long.parseLong(latestVersionFile.split("\\.")[0]))
                .orElse(-1L);
    }

    private <T> T loadConfig(String configName, String configFileName, TypeReference<T> type) {
        try {
            var config = objectMapper.readValue(new File(getConfigDir(configName), configFileName), type);

            System.out.println(config);

            return (T) config;
        } catch (IOException e) {
            log.info("Config was not found :{}", configName);
            return null;
        }
    }

    private File getConfigDir(String configName) {
        var topicDir = new File(brokerConfig.getRootDir() + "/config/" + configName);
        topicDir.mkdirs();
        return topicDir;
    }

    public int compareVersions(String file1, String file2) {
        String[] parts1 = file1.split("\\.");
        String[] parts2 = file2.split("\\.");

        long num1 = Long.parseLong(parts1[0]);
        long num2 = Long.parseLong(parts2[0]);

        return Long.compare(num1, num2);
    }

}
