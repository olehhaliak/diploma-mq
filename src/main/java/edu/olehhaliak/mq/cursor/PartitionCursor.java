package edu.olehhaliak.mq.cursor;

public interface PartitionCursor {
    AcquireCGCursorResponse acquireCGCursor(CursorDefinition request);
    ReleaseCGCursorResponse releaseCGCursor(CursorDefinition request);

    boolean isCursorHolder(CursorDefinition cursorDefinition);
}
