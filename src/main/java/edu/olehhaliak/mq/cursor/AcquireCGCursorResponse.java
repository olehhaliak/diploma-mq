package edu.olehhaliak.mq.cursor;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class AcquireCGCursorResponse {
   CursorDefinition cursorDefinition;
   int cursorPosition;
   String errCode;
}
