package edu.olehhaliak.mq.cursor;

import edu.olehhaliak.mq.PartitionId;
import lombok.Data;

@Data
public class CursorDefinition {//@TODO consider including offset here
    private String topic;
    private int partition;
    private String consumerGroup;
    private String consumerId;

    public PartitionId toPartitionId() {
        return PartitionId.builder()
                .topic(topic)
                .partition(partition)
                .build();
    }
}
