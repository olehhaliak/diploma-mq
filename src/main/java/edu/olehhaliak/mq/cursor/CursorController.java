package edu.olehhaliak.mq.cursor;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/consumer/cursor")
@RequiredArgsConstructor
public class CursorController {
    private final CursorService cursorService;

    @PostMapping("/acquire")
    private AcquireCGCursorResponse acquireCursor(@RequestBody CursorDefinition request) {
        return cursorService.ackquireCursor(request);
    }

    @PostMapping("/release")
    private ReleaseCGCursorResponse releaseCursor(@RequestBody CursorDefinition request) {
        return cursorService.releaseCursor(request);
    }
}
