package edu.olehhaliak.mq.cursor;

import edu.olehhaliak.mq.PartitionId;
import edu.olehhaliak.mq.topology.PartitionDistributionService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Service
@RequiredArgsConstructor
public class CursorService {
    private final PartitionDistributionService partitionDistributionService;
    private Map<PartitionId, PartitionCursor> partitionCursors = new ConcurrentHashMap<>();

    public AcquireCGCursorResponse ackquireCursor(CursorDefinition request) {
        var partitionId = request.toPartitionId();
        if (!isPartitionLeader(request)) {
            return AcquireCGCursorResponse.builder().errCode("NOT_A_PARTITION_LEADER").build();
        }
        return getPartitionCursor(partitionId).acquireCGCursor(request);
    }

    public ReleaseCGCursorResponse releaseCursor(CursorDefinition request) {
        if (!isPartitionLeader(request)) {
            return ReleaseCGCursorResponse.builder()
                    .success(false)
                    .err("NOT_A_PARTITION_LEADER")
                    .build();
        }
        return getPartitionCursor(request.toPartitionId()).releaseCGCursor(request);
    }

    public boolean isCursorHolder(CursorDefinition cursorDefinition) {
        return getPartitionCursor(cursorDefinition.toPartitionId()).isCursorHolder(cursorDefinition);

    }

    private PartitionCursor getPartitionCursor(PartitionId partitionId) {
        return partitionCursors.computeIfAbsent(partitionId, PartitionCursorsImpl::new);
    }

    private boolean isPartitionLeader(CursorDefinition request) {
        var partitionId = request.toPartitionId();
        return partitionDistributionService.isLeader(partitionId.getTopic(), partitionId.getPartition());

    }

}
