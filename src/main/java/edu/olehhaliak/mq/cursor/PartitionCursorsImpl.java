package edu.olehhaliak.mq.cursor;

import edu.olehhaliak.mq.PartitionId;
import lombok.RequiredArgsConstructor;

import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;

@RequiredArgsConstructor
public class PartitionCursorsImpl implements PartitionCursor {
    private Map<String, String> currentConsumerGroupConsumer = new ConcurrentHashMap<>();
    private Map<String, Integer> lastCommittedCursor = new ConcurrentHashMap<>();
    private final PartitionId partitionId;

    public AcquireCGCursorResponse acquireCGCursor(CursorDefinition request) {
        if(isCursorHolder(request)){
            return AcquireCGCursorResponse.builder()
                    .cursorDefinition(request)
                    .errCode("ALREADY_CURSOR_HOLDER")
                    .build();
        }
        if (currentConsumerGroupConsumer.get(request.getConsumerGroup()) != null) {
            return AcquireCGCursorResponse.builder()
                    .cursorDefinition(request)
                    .errCode("PARTITION_OCCUPIED")
                    .build();
        }
        currentConsumerGroupConsumer.put(request.getConsumerGroup(), request.getConsumerId());
        var cursor = getLastCommittedCursor(request.getConsumerGroup());
        return AcquireCGCursorResponse.builder()
                .cursorDefinition(request)
                .cursorPosition(cursor)
                .build();
    }

    public ReleaseCGCursorResponse releaseCGCursor(CursorDefinition request) {
        if (!isCursorHolder(request)) {
            return ReleaseCGCursorResponse.builder()
                    .success(false)
                    .err("NOT_A_CURSOR_HOLDER")
                    .build();
        }
        currentConsumerGroupConsumer.remove(request.getConsumerGroup());

        return ReleaseCGCursorResponse.builder()
                .success(true)
                .build();
    }

    @Override
    public boolean isCursorHolder(CursorDefinition cursorDefinition) {
        var currentCursorHolderId = currentConsumerGroupConsumer.get(cursorDefinition.getConsumerGroup());
        return Objects.equals(currentCursorHolderId, cursorDefinition.getConsumerId());
    }

    private int getLastCommittedCursor(String consumerGroup) {
        lastCommittedCursor.putIfAbsent(consumerGroup, 0);
        return lastCommittedCursor.get(consumerGroup);
    }
}
