package edu.olehhaliak.mq.topic.api;

import edu.olehhaliak.mq.topic.api.CreateTopicRequest;
import edu.olehhaliak.mq.topic.model.Topic;
import edu.olehhaliak.mq.topic.service.TopicService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("topic")
@RequiredArgsConstructor
public class TopicController {
    private final TopicService topicService;

    @PostMapping
    public Topic createTopic(@RequestBody CreateTopicRequest createTopicRequest){
       return topicService.createTopic(createTopicRequest);
    }

    @DeleteMapping
    public void deleteTopic(String topicName){
        topicService.deleteTopic(topicName);
    }

    @GetMapping
    public List<Topic> getTopics(){
       return topicService.getTopics();
    }
}
