package edu.olehhaliak.mq.topic.api;

import jakarta.annotation.Nonnull;
import lombok.Data;
import lombok.NonNull;

@Data
public class CreateTopicRequest {
    @Nonnull
    String name;
    @NonNull
    int partitions;
    int replicationFactor = 1;
}
