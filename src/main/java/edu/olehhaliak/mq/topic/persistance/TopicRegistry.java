package edu.olehhaliak.mq.topic.persistance;

import com.fasterxml.jackson.databind.ObjectMapper;
import edu.olehhaliak.mq.config.BrokerConfig;
import edu.olehhaliak.mq.config.util.VersionedGenericConfigPersister;
import edu.olehhaliak.mq.topic.model.Partition;
import edu.olehhaliak.mq.topic.model.Topic;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import com.fasterxml.jackson.core.type.TypeReference;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class TopicRegistry {
    private final ObjectMapper objectMapper;
    private final BrokerConfig brokerConfig;
    private final VersionedGenericConfigPersister configPersister;

    private Map<String, Topic> topics = new ConcurrentHashMap<>();
    private long lastVersion = System.currentTimeMillis();
    private int lastHash;

    @PostConstruct
    public void init() {
        configPersister.loadLatestConfig("topics", new TypeReference<Map<String, Topic>>() {})
                .ifPresent(this::loadConfig);
    }

    private void loadConfig(Map<String, Topic> topics) {
        this.topics = topics;
        lastHash = topics.hashCode();
        lastVersion = configPersister.getLatestVersion("topics");
    }

    public Map<String, Topic> getTopics() {
        return new HashMap<>(topics);
    }

    public Topic getTopicByName(String topicName) {
        return this.topics.get(topicName);
    }

    public Set<Partition> getPartitions() {
        return topics.values().stream()
                .map(Topic::getPartitions)
                .flatMap(Set::stream)
                .collect(Collectors.toSet());
    }

    public long getLastVersion() {
        return lastVersion;
    }

    public void addTopic(Topic topic) {
        var existingTopic = topics.get(topic.getName());
        if (existingTopic != null && existingTopic.getParams().equals(topic.getParams())) {
            return;
        }
        topics.put(topic.getName(), topic);
        persistTopics();
    }

    public void removeTopic(String topicName) {
        topics.remove(topicName);
        persistTopics();
    }

    private void persistTopics() {
        if (topics.hashCode() == lastHash) {
            return;
        }

        lastVersion = configPersister.persistConfig("topics", topics);
        lastHash = topics.hashCode();
    }

//    private File getTopicConfigDir() {
//        var topicDir = new File(brokerConfig.getRootDir()+ "/config/topics");
//        topicDir.mkdirs();
//        return topicDir;
//    }

//    public int compare(String file1, String file2) {
//        String[] parts1 = file1.split("\\.");
//        String[] parts2 = file2.split("\\.");
//
//        long num1 = Long.parseLong(parts1[0]);
//        long num2 = Long.parseLong(parts2[0]);
//
//        return Long.compare(num1, num2);
//    }
}
