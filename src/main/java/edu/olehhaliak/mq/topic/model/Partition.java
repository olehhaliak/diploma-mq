package edu.olehhaliak.mq.topic.model;

import lombok.*;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class Partition {
    private int shardId;
    private String topicName;
    private int partitionNumber;
    private boolean isLeader;
}
