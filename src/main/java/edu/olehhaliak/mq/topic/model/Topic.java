package edu.olehhaliak.mq.topic.model;

import lombok.*;

import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode
public class Topic {
    private String name;
    private Set<Partition> partitions;
    private TopicParams params;
}
