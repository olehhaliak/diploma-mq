package edu.olehhaliak.mq.topic.model;

import lombok.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class TopicParams {
    int partitions;
    int replicationFactor;
}
