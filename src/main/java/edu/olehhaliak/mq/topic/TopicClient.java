package edu.olehhaliak.mq.topic;

import edu.olehhaliak.mq.topic.api.CreateTopicRequest;
import edu.olehhaliak.mq.topic.model.Topic;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
@RequiredArgsConstructor
public class TopicClient {
    private final RestTemplate restTemplate;

    public Topic createTopic(CreateTopicRequest request, String leaderUrl){
        var url = leaderUrl + "/topic";
        return restTemplate.postForObject(url, request, Topic.class);
    }
}
