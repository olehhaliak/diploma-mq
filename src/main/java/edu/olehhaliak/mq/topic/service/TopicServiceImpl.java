package edu.olehhaliak.mq.topic.service;

import edu.olehhaliak.mq.discoverability.registry.BrokerRegistryService;
import edu.olehhaliak.mq.topic.TopicClient;
import edu.olehhaliak.mq.topic.api.CreateTopicRequest;
import edu.olehhaliak.mq.topic.model.Topic;
import edu.olehhaliak.mq.topic.persistance.TopicRegistry;
import edu.olehhaliak.mq.topology.PartitionDistributionService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
@AllArgsConstructor
public class TopicServiceImpl implements TopicService {
    private final TopicRegistry topicRegistry;
    private final PartitionDistributionService partitionDistributionService;
    private final BrokerRegistryService brokerRegistryService;
    private final TopicClient topicClient;

    @Override
    public Topic createTopic(CreateTopicRequest request) {
        if (brokerRegistryService.isCoordinator()) {
            return coordinatorCreateTopic(request);
        } else {
            return rerouteToCoordinator(request);
        }
    }

    private Topic rerouteToCoordinator(CreateTopicRequest request) {
        log.info("Reroute {}", request);
        var coordinatorURL = brokerRegistryService.getRegistry().getBrokers().get(brokerRegistryService.getRegistry().getCoordinatorId());
        return topicClient.createTopic(request, coordinatorURL);
    }

    private Topic coordinatorCreateTopic(CreateTopicRequest request) {
        var topic = TopicAssembler.assembleTopic(request);
        if (topicRegistry.getTopics().containsKey(topic.getName())) {
            log.warn("Topic Already exists");
            return topicRegistry.getTopicByName(topic.getName());
        }
        topicRegistry.addTopic(topic);
        partitionDistributionService.distributeNewTopic(topic);
        return topic;
    }


    @Override
    public void deleteTopic(String topicName) {
        topicRegistry.removeTopic(topicName);
    }

    @Override
    public List<Topic> getTopics() {
        return topicRegistry.getTopics().values().stream().toList();
    }
}
