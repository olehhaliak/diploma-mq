package edu.olehhaliak.mq.topic.service;

import edu.olehhaliak.mq.topic.api.CreateTopicRequest;
import edu.olehhaliak.mq.topic.model.Partition;
import edu.olehhaliak.mq.topic.model.Topic;
import edu.olehhaliak.mq.topic.model.TopicParams;
import lombok.experimental.UtilityClass;

import java.util.HashSet;

@UtilityClass
public class TopicAssembler {

    public static Topic assembleTopic(CreateTopicRequest request) {
        var partitions = new HashSet<Partition>();
        for (int i = 0; i < request.getPartitions(); i++) {
            for (int j = 0; j < request.getReplicationFactor(); j++) {
                var partition = Partition.builder()
                        .shardId(j)
                        .topicName(request.getName())
                        .partitionNumber(i)
                        .isLeader(j == 0)
                        .build();

                partitions.add(partition);
            }
        }

        var topicParams = TopicParams.builder()
                .partitions(request.getPartitions())
                .replicationFactor(request.getReplicationFactor())
                .build();
        return Topic.builder()
                .name(request.getName())
                .partitions(partitions)
                .params(topicParams)
                .build();
    }
}