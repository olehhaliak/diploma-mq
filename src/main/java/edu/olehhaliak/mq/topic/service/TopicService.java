package edu.olehhaliak.mq.topic.service;

import edu.olehhaliak.mq.topic.api.CreateTopicRequest;
import edu.olehhaliak.mq.topic.model.Topic;

import java.util.List;

public interface TopicService {

    Topic createTopic(CreateTopicRequest topic);

    void deleteTopic(String topicName);

    List<Topic> getTopics();
}
