package edu.olehhaliak.mq.persistance;

import edu.olehhaliak.mq.PartitionId;
import edu.olehhaliak.mq.persistance.log.Log;

public interface IndexManager {

    Log append(Log log);
    Log readLog(PartitionId partitionId, int offset);
}
