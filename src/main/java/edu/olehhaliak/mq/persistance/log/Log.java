package edu.olehhaliak.mq.persistance.log;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Log {
    private String topic;
    private Integer partition;
    private Integer offset;
    private Long unixTimestamp;
    private String key;
    private String value;
}
