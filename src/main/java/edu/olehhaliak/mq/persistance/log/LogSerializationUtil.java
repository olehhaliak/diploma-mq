package edu.olehhaliak.mq.persistance.log;

import edu.olehhaliak.mq.persistance.log.Log;
import edu.olehhaliak.mq.persistance.log.LogMetadata;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;

public class LogSerializationUtil {

    public static byte[] serialize(Log log) {
        var key = log.getKey().getBytes(StandardCharsets.UTF_8);
        var payload = log.getValue().getBytes(StandardCharsets.UTF_8);
        return ByteBuffer.allocate(
                        4 + //offset
                                8 + //timestamp
                                4 + // key size
                                4 + // payload size
                                key.length +
                                payload.length)
                .putInt(0, log.getOffset())
                .putLong(4, log.getUnixTimestamp())
                .putInt(12, key.length)
                .putInt(16, payload.length)
                .put(20, key)
                .put(20 + key.length, payload)
                .array();
    }

    public static LogMetadata toMetadata(byte[] bytes) {
        ByteBuffer buffer = ByteBuffer.wrap(bytes);
        return new LogMetadata(
                buffer.getInt(),
                buffer.getLong(),
                buffer.getInt(),
                buffer.getInt()
        );
    }

    public static Log toLog(LogMetadata logMetadata, byte[] kv) {
        ByteBuffer buffer = ByteBuffer.wrap(kv);
        byte[] keyBytes = new byte[logMetadata.getKeySize()];
        buffer.get(keyBytes);
        byte[] payloadBytes = new byte[logMetadata.getPayloadSize()];
        buffer.get(payloadBytes);

        String key = new String(keyBytes, StandardCharsets.UTF_8);
        String value = new String(payloadBytes, StandardCharsets.UTF_8);

        return Log.builder()
                .offset(logMetadata.getOffset())
                .unixTimestamp(logMetadata.getUnixTimestamp())
                .key(key)
                .value(value)
                .build();
    }
}
