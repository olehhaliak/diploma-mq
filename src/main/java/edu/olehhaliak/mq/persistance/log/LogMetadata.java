package edu.olehhaliak.mq.persistance.log;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LogMetadata {
    private int offset;
    private long unixTimestamp;
    private int keySize;
    private int payloadSize;
}
