package edu.olehhaliak.mq.persistance;

import edu.olehhaliak.mq.persistance.log.Log;

import java.util.HashMap;


/**
 * Stores all message logs within a certain partition
 */
public interface PartitionIndex {

    Log append(Log log);
    Log readLog(int offset);

}
