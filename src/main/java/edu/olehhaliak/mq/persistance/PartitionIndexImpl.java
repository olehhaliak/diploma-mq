package edu.olehhaliak.mq.persistance;

import edu.olehhaliak.mq.persistance.log.Log;

import java.io.File;
import java.util.*;

public class PartitionIndexImpl implements PartitionIndex {
    public static final int MAX_FILE_SIZE = 100;
    private static final String LOGDIR_ROOT = "./log";//@TODO config
    private final File partitionDir;
    private final int maxFileSize;
    private final Map<Integer, FileIndex> fileIndexes = new TreeMap<>();
    private FileIndex currentFileIndex;

    public PartitionIndexImpl(String logdirRoot,String topicName, int partitionNumber) {
        this.partitionDir = new File(logdirRoot + "/" + topicName + "/" + partitionNumber);
        this. partitionDir.mkdirs();
        this.maxFileSize = MAX_FILE_SIZE;
        loadFileIndexes();
        currentFileIndex = fileIndexes.entrySet().stream()
                .max(Map.Entry.comparingByKey())
                .map(Map.Entry::getValue)
                .orElseGet(()->createNewFileIndex(0));
    }

    @Override
    public Log append(Log log) {
        if (currentFileIndex.getCurrentFilePos() >= maxFileSize) {
            currentFileIndex = createNewFileIndex(currentFileIndex.getLatestOffset());
        }
        return currentFileIndex.append(log);
    }

    @Override
    public Log readLog(int offset) {
        var fileIndex = getFileIndex(offset);
        if (fileIndex != null) {
            return fileIndex.readLog(offset);
        }
        throw new RuntimeException("no matching File Index found");
    }


    private FileIndexImpl createNewFileIndex(int offset) {
        FileIndexImpl newFileIndex = new FileIndexImpl(partitionDir, offset);
        fileIndexes.put(offset, newFileIndex);
        return newFileIndex;
    }


    private void loadFileIndexes() {
        Optional.ofNullable(partitionDir.list((d, name) -> name.endsWith(".idx")))
                .stream()
                .flatMap(Arrays::stream)
                .map(fileName-> Integer.parseInt(fileName.replace(".idx", "")))
                .forEach(this::loadFileIndex);
    }

    private void loadFileIndex(int fileOffset) {
        FileIndex fileIndex = new FileIndexImpl(partitionDir, fileOffset);
        fileIndexes.put(fileOffset, fileIndex);
    }

    private FileIndex getFileIndex(int offset) {
        return fileIndexes.entrySet().stream()
                .filter(entry -> entry.getKey() <= offset)
                .max(Map.Entry.comparingByKey())
                .map(Map.Entry::getValue)
                .orElse(null);
    }
}
