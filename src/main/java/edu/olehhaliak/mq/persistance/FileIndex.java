package edu.olehhaliak.mq.persistance;

import edu.olehhaliak.mq.persistance.log.Log;

public interface FileIndex {
    Log append(Log log);
    Log readLog(int offset);

    /**
     *  return latest offset, mentioned withing this file index
     * @return
     */
    int getLatestOffset();

    int getCurrentFilePos();
}