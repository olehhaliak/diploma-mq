package edu.olehhaliak.mq.persistance;

import edu.olehhaliak.mq.PartitionId;
import edu.olehhaliak.mq.persistance.log.Log;
import lombok.RequiredArgsConstructor;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@RequiredArgsConstructor
public class IndexManagerImpl implements IndexManager {
    private final String logdir;
    Map<String, Map<Integer, PartitionIndex>> partitionsMap = new HashMap<>(); // replace with PartitionId

    @Override
    public Log append(Log log) {
        var pi = getPartitionIndex(log);
        return pi.append(log);
    }

    private PartitionIndex getPartitionIndex(Log log) {
        var topicPartitionMap = partitionsMap.get(log.getTopic());
        return Optional.ofNullable(partitionsMap.get(log.getTopic()))
                .map(topicPartitions -> topicPartitions.get(log.getPartition()))
                .orElseGet(() -> createNewPartitionIndex(log.getTopic(), log.getPartition()));
    }

    private PartitionIndex createNewPartitionIndex(String topic, int partition) {
        var newPartitionIndex = new PartitionIndexImpl(logdir,topic, partition);
        if (!partitionsMap.containsKey(topic)) {
            partitionsMap.put(topic, new HashMap<>());
        }
        partitionsMap.get(topic).put(partition, newPartitionIndex);
        return newPartitionIndex;
    }

    @Override
    public Log readLog(PartitionId partitionId, int offset){//@TODO refactor to check if partiton exist
        var log = Log.builder().topic(partitionId.getTopic()).partition(partitionId.getPartition()).build();

        var logMsg = getPartitionIndex(log).readLog(offset);
        logMsg.setTopic(partitionId.getTopic());
        logMsg.setPartition(partitionId.getPartition());
        return logMsg;
    }
}
