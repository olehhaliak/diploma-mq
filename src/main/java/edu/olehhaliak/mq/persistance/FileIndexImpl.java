package edu.olehhaliak.mq.persistance;

import edu.olehhaliak.mq.persistance.log.Log;
import edu.olehhaliak.mq.persistance.log.LogMetadata;
import edu.olehhaliak.mq.persistance.log.LogSerializationUtil;

import java.io.*;
import java.nio.ByteBuffer;
import java.util.HashMap;

public class FileIndexImpl implements FileIndex {
    private final File indexFile;
    private final File logFile;
    private final HashMap<Integer, Integer> offsetFilePositions = new HashMap<>();
    private int currentOffset;
    private int currentFilePos;

    public FileIndexImpl(File parentDir, int fileOffset) {

        this.indexFile = new File(parentDir,fileOffset + ".idx");
        this.logFile = new File(parentDir,fileOffset + ".log");

        if (indexFile.exists()) {
            readIndex();
            currentFilePos = (int) logFile.length();
            currentOffset = offsetFilePositions.keySet().stream()
                    .mapToInt(Integer::intValue)
                    .max()
                    .orElse(fileOffset - 1) + 1;
        }else {
            currentOffset = fileOffset;
        }
    }

    @Override
    public int getCurrentFilePos() {
        return currentFilePos;
    }


    @Override
    public int getLatestOffset() {
        return currentOffset;
    }

    @Override
    public Log append(Log log) {
        fillInMissingLogInfo(log);
        var bytes = LogSerializationUtil.serialize(log); //@TODO: move to a serializer class
        appendLogToFile(bytes);
        appendIndex(log.getOffset(), currentFilePos);
        currentFilePos += bytes.length;
        return log;
    }

    @Override
    public Log readLog(int offset) {
        Integer filePos = offsetFilePositions.get(offset);
        if (filePos == null) {
            return null; // or throw an exception if the offset does not exist
        }

        try (var raf = new RandomAccessFile(logFile, "r")) {
            raf.seek(filePos);

            byte[] header = new byte[20];
            if (raf.read(header) != 20) {
                throw new IOException("Failed to read log header");
            }

            LogMetadata logMetadata = LogSerializationUtil.toMetadata(header);

            byte[] kv = new byte[logMetadata.getKeySize() + logMetadata.getPayloadSize()];
            if (raf.read(kv) != kv.length) {
                throw new IOException("Failed to read log key-value");
            }

            return LogSerializationUtil.toLog(logMetadata, kv);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void fillInMissingLogInfo(Log log) {
        log.setOffset(getNextOffset());
        log.setUnixTimestamp(System.currentTimeMillis());
    }

    private int getNextOffset() {
        return currentOffset++;
    }

    private void readIndex() {
        try (var fis = new FileInputStream(indexFile)) {
            var buffer = new byte[8];
            while (fis.read(buffer) == 8) {
                var wrapped = ByteBuffer.wrap(buffer);
                var offset = wrapped.getInt();
                var filePos = wrapped.getInt();
                offsetFilePositions.put(offset, filePos);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void appendIndex(int offset, int filePos) {
        appendIndexToFile(offset, filePos);
        appendIndexToLocalCache(offset, filePos);
    }

    private void appendIndexToLocalCache(int offset, int filePos) {
        offsetFilePositions.put(offset, filePos);
    }

    private void appendIndexToFile(int offset, int filePos) {
        try (var fos = new FileOutputStream(indexFile, true)) {
            var buffer = ByteBuffer.allocate(8);
            buffer.putInt(offset);
            buffer.putInt(filePos);
            fos.write(buffer.array());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void appendLogToFile(byte[] bytes) {
        try (var fos = new FileOutputStream(logFile, true)) {
            fos.write(bytes);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
