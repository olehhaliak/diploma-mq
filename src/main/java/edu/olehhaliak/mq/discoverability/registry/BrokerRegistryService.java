package edu.olehhaliak.mq.discoverability.registry;

import edu.olehhaliak.mq.config.BrokerConfig;
import jakarta.annotation.PostConstruct;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@RequiredArgsConstructor
public class BrokerRegistryService {

    private final BrokerConfig brokerConfig;
    private BrokerRegistry brokerRegistry = new BrokerRegistry();
    private long lastUpdated = System.currentTimeMillis();

    @PostConstruct
    public void init(){
        brokerRegistry.addBroker(brokerConfig.getBrokerId(), brokerConfig.getUrl());
        if(brokerConfig.isStandalone()){
            brokerRegistry.setCoordinatorId(brokerConfig.getBrokerId());
        }
        registryUpdated();
    }

    public void registerBroker(String brokerId, String brokerUrl) {
        brokerRegistry.addBroker(brokerId, brokerUrl);
        log.info("Registered broker with ID: {} and URL: {}", brokerId, brokerUrl);
        registryUpdated();
    }

    public void updateRegistry(BrokerRegistry registry) {
        this.brokerRegistry = registry;
        log.info("registry updated");
        log.debug("current registry: {}", registry);
        registryUpdated();
    }

    public void removeBroker(String brokerId) {
        brokerRegistry.removeBroker(brokerId);
        log.info("Removed broker with ID: {}", brokerId);
        registryUpdated();
    }

    public BrokerRegistry getRegistry() {
        return brokerRegistry;
    }

    public boolean isCoordinator(){
        return brokerConfig.getBrokerId().equals(brokerRegistry.getCoordinatorId());
    }

    public long getLastUpdateTime(){
        return lastUpdated;
    }
    private void registryUpdated(){
        lastUpdated = System.currentTimeMillis();
    }


}
