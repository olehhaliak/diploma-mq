package edu.olehhaliak.mq.discoverability.registry;

import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.springframework.stereotype.Component;
import lombok.extern.slf4j.Slf4j;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Slf4j
@Component
@ToString
@EqualsAndHashCode
public class BrokerRegistry {

    private final Map<String, String> brokers = new ConcurrentHashMap<>();
    private  String coordinatorId;

    public void addBroker(String brokerId, String brokerUrl) {
        brokers.put(brokerId, brokerUrl);
        log.info("Broker {} added to registry with URL {}", brokerId, brokerUrl);
    }

    public void removeBroker(String brokerId) {
        brokers.remove(brokerId);
        log.info("Broker {} removed from registry", brokerId);
    }

    public void setCoordinatorId(String coordinatorId){
        this.coordinatorId = coordinatorId;

    }
    public String getCoordinatorId() {
        return coordinatorId;
    }

    public Map<String, String> getBrokers() {
        return brokers;
    }
}
