package edu.olehhaliak.mq.discoverability;

import edu.olehhaliak.mq.discoverability.coordinator.CoordinatorService;
import edu.olehhaliak.mq.discoverability.coordinator.SubscribeRequest;
import edu.olehhaliak.mq.discoverability.election.Candidate;
import edu.olehhaliak.mq.discoverability.election.CoordinatorUpdateAnnouncement;
import edu.olehhaliak.mq.discoverability.election.ElectionsService;
import edu.olehhaliak.mq.discoverability.follower.CoordinatorHeartbeatService;
import edu.olehhaliak.mq.discoverability.follower.FollowerService;
import edu.olehhaliak.mq.discoverability.registry.BrokerRegistry;
import edu.olehhaliak.mq.discoverability.registry.BrokerRegistryService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/cluster")
@RequiredArgsConstructor
@Slf4j
public class ClusterController {

    private final BrokerRegistryService brokerRegistryService;
    private final CoordinatorService coordinatorService;
    private final CoordinatorHeartbeatService coordinatorHeartbeatService;
    private final FollowerService followerService;
    private final ElectionsService electionsService;

    @GetMapping("/registry")
    public BrokerRegistry getBrokerRegistry(){
        return brokerRegistryService.getRegistry();
    }

    @PutMapping("/registry")
    public void  updateRegistry(@RequestBody BrokerRegistry registry){
        brokerRegistryService.updateRegistry(registry);
    }

    @PostMapping("/coordinator/subscribe")
    public void register(@RequestBody SubscribeRequest subscribeRequest){
        coordinatorService.addSubscriber(subscribeRequest);
    }

    @PostMapping("/follower/announce")
    public void announceNewCoordinator(@RequestBody CoordinatorUpdateAnnouncement coordinatorUpdateAnnouncement){
        followerService.newCoordinatorAnnounced(coordinatorUpdateAnnouncement);
    }


    @GetMapping("/follower/heartbeat")
    public void followerHeartbeat(){
        coordinatorHeartbeatService.coordinatorHeartbeatReceived();
    }

    @PostMapping("/election/start")
    public void startElection(@RequestBody Candidate candidate){
        electionsService.candidateReceived(candidate);
    }

    @PostMapping("/election/vote")
    public void receiveVote(@RequestBody String voterBrokerId){
        electionsService.addVoteForMyself(voterBrokerId);
    }


}
