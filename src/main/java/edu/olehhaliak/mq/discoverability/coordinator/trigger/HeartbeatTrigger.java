package edu.olehhaliak.mq.discoverability.coordinator.trigger;

import edu.olehhaliak.mq.discoverability.coordinator.FollowerHeartbeats;
import edu.olehhaliak.mq.discoverability.registry.BrokerRegistryService;
import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class HeartbeatTrigger {
    private final FollowerHeartbeats followerHeartbeats;
    private final BrokerRegistryService brokerRegistryService;

    @Scheduled(fixedRate = 5000)
    public void runHeartbeats() {
        if (brokerRegistryService.isCoordinator()) {
            followerHeartbeats.sendHeartbeats();
        }
    }

    @Scheduled(fixedRate = 5000)
    public void checkForHeartbeatTimeouts() {
        if (brokerRegistryService.isCoordinator()) {
            followerHeartbeats.checkForTimeouts();
        }
    }
}
