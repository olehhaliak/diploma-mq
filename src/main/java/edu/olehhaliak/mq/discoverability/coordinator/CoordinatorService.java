package edu.olehhaliak.mq.discoverability.coordinator;

import edu.olehhaliak.mq.discoverability.election.ElectionStage;
import edu.olehhaliak.mq.discoverability.election.ElectionsService;
import edu.olehhaliak.mq.discoverability.registry.BrokerRegistryService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@Service
@Slf4j
@RequiredArgsConstructor
public class CoordinatorService {
    private final BrokerRegistryService brokerRegistryService;
    private final RestTemplate restTemplate;
    public void addSubscriber(SubscribeRequest request){
        brokerRegistryService.registerBroker(request.refererName, request.url);
    }

    @Async
    public void propagateBrokerRegistry() {
       log.info("Propagating broker registry to followers");
       getFollowers().values().forEach(this::propagateBRToFollower);
    }

    public Map<String, String> getFollowers() {
        var followers = new HashMap<>(brokerRegistryService.getRegistry().getBrokers());
        followers.remove(brokerRegistryService.getRegistry().getCoordinatorId());
        return followers;
    }



    @Async
    protected void propagateBRToFollower(String followerURL) {
        var uri = followerURL + "/cluster/registry";
        restTemplate.put(uri, brokerRegistryService.getRegistry());
    }
}
