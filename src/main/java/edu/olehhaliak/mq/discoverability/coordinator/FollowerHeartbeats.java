package edu.olehhaliak.mq.discoverability.coordinator;

import edu.olehhaliak.mq.discoverability.registry.BrokerRegistryService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Predicate;

@Service
@Slf4j
@RequiredArgsConstructor
public class FollowerHeartbeats {
    private final BrokerRegistryService brokerRegistryService;
    private final CoordinatorService coordinatorService;
    private final RestTemplate restTemplate;
    private final long TIMEOUT_MILLIS = 15_000;
    Map<String, Long> heartBeats = new ConcurrentHashMap<>();

    public void sendHeartbeats(){
        var followers = coordinatorService.getFollowers();
        //put missing followers into map
        followers.keySet().stream()
                .filter(Predicate.not(heartBeats::containsKey))
                .forEach(follower -> heartBeats.put(follower, System.currentTimeMillis()));

        followers.forEach(this::sendHeartBeat);
    }

    @Async
    protected void sendHeartBeat(String followerId, String followerUrl) {
        var url = followerUrl + "/cluster/follower/heartbeat";
        try {
            var response = restTemplate.getForEntity(url, Void.class);
            if (response.getStatusCode().is2xxSuccessful()) {
                heartBeats.put(followerId, System.currentTimeMillis());
            }
        }
        catch(Exception e){
            log.info("heartbeat failed for follower {} {}",followerId, followerUrl);
        }
    }


    public void checkForTimeouts() {
        var currentTime = System.currentTimeMillis();
        heartBeats.entrySet().stream()
                .filter(e->currentTime - e.getValue() > TIMEOUT_MILLIS )
                .map(Map.Entry::getKey)
                .forEach(this::declareTimeout);

    }

    private void declareTimeout(String followerId) {
        log.info("Follower {} does not respond to heartbeats anymore", followerId);
        brokerRegistryService.removeBroker(followerId);
        heartBeats.remove(followerId);
    }
}
