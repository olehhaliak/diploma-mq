package edu.olehhaliak.mq.discoverability.coordinator.trigger;

import edu.olehhaliak.mq.discoverability.coordinator.CoordinatorService;
import edu.olehhaliak.mq.discoverability.registry.BrokerRegistryService;
import edu.olehhaliak.mq.topology.PartitionDistributionService;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
@Component
@RequiredArgsConstructor
@Slf4j
public class CoordinatorRegistryUpdateTrigger {
    private final BrokerRegistryService brokerRegistryService;
    private final CoordinatorService coordinatorService;
    private final PartitionDistributionService partitionDistributionService;
    int lastHash;
    @PostConstruct
    public void init(){
       lastHash = brokerRegistryService.getRegistry().hashCode();
    }
    @Scheduled(fixedRate = 5000)
    public void checkForRegistryUpdate(){
        var hash = brokerRegistryService.getRegistry().hashCode();
        if(brokerRegistryService.isCoordinator() && hash != lastHash) {
            lastHash = hash;
            log.info("Registry update detected, propagating to followers");
            onRegistryUpdated();
        }
    }

    private void onRegistryUpdated(){
        coordinatorService.propagateBrokerRegistry();
        partitionDistributionService.brokerRegistryUpdated();
    }
}
