package edu.olehhaliak.mq.discoverability.election;

public enum ElectionStage {
    NOT_STARTED,
    REGISTERING_CANDIDATES,
    VOTING
}
