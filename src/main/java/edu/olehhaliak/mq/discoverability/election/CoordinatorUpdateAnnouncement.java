package edu.olehhaliak.mq.discoverability.election;

import lombok.*;
import org.springframework.scheduling.annotation.Async;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class CoordinatorUpdateAnnouncement {
    private String newCoordinatorId;
    private String newCoordinatorUrl;
}
