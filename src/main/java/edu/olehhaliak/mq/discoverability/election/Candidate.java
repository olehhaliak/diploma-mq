package edu.olehhaliak.mq.discoverability.election;

import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class Candidate {
    private String brokerId;
    private String url;
    private long lastUpdate;


}
