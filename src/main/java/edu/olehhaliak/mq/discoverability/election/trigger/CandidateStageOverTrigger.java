package edu.olehhaliak.mq.discoverability.election.trigger;

import edu.olehhaliak.mq.discoverability.election.ElectionStage;
import edu.olehhaliak.mq.discoverability.election.ElectionsService;
import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class CandidateStageOverTrigger {
    private final ElectionsService electionsService;
    private final long CANDIDATE_STAGE_TIMEOUT= 10_000;

    @Scheduled(fixedDelay = 1_000)
    public void checkIfCandidateStageOver(){
        if(electionsService.getElectionStage() == ElectionStage.REGISTERING_CANDIDATES && candidateStageTimeoutHit() ) {
            electionsService.startVoting();
        }
    }

    private boolean candidateStageTimeoutHit() {
        var currentTime = System.currentTimeMillis();
       return currentTime - electionsService.getCandidateStageTimeout() > CANDIDATE_STAGE_TIMEOUT;
    }

}
