package edu.olehhaliak.mq.discoverability.election;

import edu.olehhaliak.mq.config.BrokerConfig;
import edu.olehhaliak.mq.discoverability.registry.BrokerRegistryService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import static edu.olehhaliak.mq.discoverability.election.ElectionStage.NOT_STARTED;
import static edu.olehhaliak.mq.discoverability.election.ElectionStage.REGISTERING_CANDIDATES;
import static java.util.Comparator.comparing;

@Service
@Slf4j
@RequiredArgsConstructor
public class ElectionsService {
    private static final double ELECTION_WIN_CONSENSUS_THRESHOLD = 0.5;
    private final BrokerRegistryService brokerRegistryService;
    private final BrokerConfig brokerConfig;
    private final ElectionClient electionClient;
    private volatile ElectionStage electionStage = NOT_STARTED;

    // REGISTERING_CANDIDATES stage
    private Map<String, Candidate> candidates = new ConcurrentHashMap<>();
    private long candidateStageStart;

    // VOTING stage
    private Set<String> myVotersBrokerIds = new HashSet<>();

    public synchronized void startElectionIfNotExists() {
        if (electionStage == NOT_STARTED) {
            initNewElection();
        }
    }

    public void candidateReceived(Candidate candidate) {
        startElectionIfNotExists();
        if (electionStage == REGISTERING_CANDIDATES) {
            registerCandidate(candidate);
        } else {
            log.info("Candidate {} received, but will be ignored since candidate registration is over", candidate.getBrokerId());
        }
    }

    public void startVoting() {
        log.info("Start voting");
        electionStage = ElectionStage.VOTING;
        log.info("Candidates registered : {}", candidates);
        var winner = evaluateCandidates();
        log.info("My vote will be sent to: {}", winner);
        voteForWinner(winner);
    }

    public void electionsAreOver() {
        electionStage = NOT_STARTED;
        candidates = new ConcurrentHashMap<>();
        myVotersBrokerIds = new HashSet<>();
    }

    private void voteForWinner(Candidate winner) {
        var myBrokerId = brokerConfig.getBrokerId();
        if (winner.getBrokerId().equals(myBrokerId)) {
            addVoteForMyself(myBrokerId);
        } else {
            sendMyVoteToWinner(myBrokerId, winner);
        }
    }

    public synchronized void addVoteForMyself(String voterBrokerId) {
        log.info("Vote for myself received from {}", voterBrokerId);
        myVotersBrokerIds.add(voterBrokerId);
        checkForCoordinatorThreshold();
    }

    private void checkForCoordinatorThreshold() {
        if (myVotersBrokerIds.size() * 1. / candidates.size() > ELECTION_WIN_CONSENSUS_THRESHOLD) {
            log.warn("Election win threshold passed, assuming coordinatorship..");
            //remove old coordinator
            brokerRegistryService.getRegistry().removeBroker(brokerRegistryService.getRegistry().getCoordinatorId());
            //add any new brokers from candidates
            brokerRegistryService.getRegistry().setCoordinatorId(brokerConfig.getBrokerId());
            announceNewCoordinator();
        }
    }

    private void announceNewCoordinator() {
        candidates.values().forEach(
                candidate -> brokerRegistryService.getRegistry().addBroker(candidate.getBrokerId(), candidate.getUrl())
        );


        candidates.values().stream()
                .filter(candidate -> !candidate.getBrokerId().equals(brokerConfig.getBrokerId())) //remove myself
                .map(Candidate::getUrl)
                .forEach(this::sendAnnouncement);
    }

    private void sendAnnouncement(String followerUrl) {
        var announcement = CoordinatorUpdateAnnouncement.builder()
                .newCoordinatorId(brokerConfig.getBrokerId())
                .newCoordinatorUrl(brokerConfig.getUrl())
                .build();
        electionClient.announceLeader(announcement,followerUrl);
    }

    private void sendMyVoteToWinner(String myBrokerId, Candidate winner) {
        electionClient.sendVoteToWinner(myBrokerId, winner.getUrl());
    }

    private Candidate evaluateCandidates() {
        var candidateLeader = candidates.get(brokerRegistryService.getRegistry().getCoordinatorId());
        if (candidateLeader != null) {
            return candidateLeader;
        }
        return candidates.values().stream()
                .max(comparing(Candidate::getLastUpdate))
                .orElseThrow(() -> new IllegalStateException("there were no candidate, not event currend broker"));
    }

    private void initNewElection() {
        log.info("Initiating new election");
        electionStage = REGISTERING_CANDIDATES;
        candidateStageStart = System.currentTimeMillis();
        candidates = new ConcurrentHashMap<String, Candidate>();
        var candidate = Candidate.builder()
                .brokerId(brokerConfig.getBrokerId())
                .url(brokerConfig.getUrl())
                .lastUpdate(brokerRegistryService.getLastUpdateTime())
                .build();
        registerCandidate(candidate);
        offerCandidateToOther(candidate);
    }

    protected void offerCandidateToOther(Candidate candidate) {
        var brokers = new HashMap<>(brokerRegistryService.getRegistry().getBrokers());
        brokers.remove(brokerConfig.getBrokerId()); //remove current broker

        brokers.values().stream()
                .peek(u -> log.info("Send to {}", u))
                .forEach(url -> electionClient.offerCandidacy(url, candidate));

    }


    private void registerCandidate(Candidate candidate) {
        log.info("Candidate registered: {}", candidate.getBrokerId());
        candidates.put(candidate.getBrokerId(), candidate);
    }

    public ElectionStage getElectionStage() {
        return electionStage;
    }

    public long getCandidateStageTimeout() {
        return candidateStageStart;
    }
}
