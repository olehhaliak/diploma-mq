package edu.olehhaliak.mq.discoverability.election.trigger;

import edu.olehhaliak.mq.discoverability.election.ElectionStage;
import edu.olehhaliak.mq.discoverability.election.ElectionsService;
import edu.olehhaliak.mq.discoverability.follower.CoordinatorHeartbeatService;
import edu.olehhaliak.mq.discoverability.registry.BrokerRegistryService;
import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class CoordinatorDownTrigger {
   private final BrokerRegistryService brokerRegistryService;
   private final CoordinatorHeartbeatService coordinatorHeartbeatService;
   private final ElectionsService electionsService;

    @Scheduled(fixedRate = 5000)
    public void assessCoordinatorState(){
        if(!brokerRegistryService.isCoordinator() && electionsService.getElectionStage() == ElectionStage.NOT_STARTED){
            coordinatorHeartbeatService.assessCoordinatorHeartBeat();
        }
    }
}
