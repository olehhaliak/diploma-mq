package edu.olehhaliak.mq.discoverability.election;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
@RequiredArgsConstructor
@Slf4j
public class ElectionClient {
    private final RestTemplate restTemplate;
    @Async
    public void offerCandidacy(String brokerUrl, Candidate candidate) {
        try {
            var url = brokerUrl + "/cluster/election/start";
            restTemplate.postForEntity(url, candidate, Void.class);
        }catch (Exception e){
            log.info("Cannot offer candidacy to the broker: {}", brokerUrl);
        }

    }

    @Async
    public void sendVoteToWinner(String myBrokerId, String winnerUrl){
        var url = winnerUrl + "/cluster/election/vote";
        restTemplate.postForEntity(url, myBrokerId,Void.class);
    }

    @Async
    public void announceLeader(CoordinatorUpdateAnnouncement announcement, String followerUrl) {
        var url = followerUrl + "/cluster/follower/announce";
        restTemplate.postForEntity(url,announcement,Void.class);
    }
}
