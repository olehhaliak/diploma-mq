package edu.olehhaliak.mq.discoverability.follower;

import edu.olehhaliak.mq.discoverability.election.ElectionsService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@Slf4j
public class CoordinatorHeartbeatService {
    private static final long COORDINATOR_HEARTBEAT_TIMEOUT = 15_000;
    long lastCoordinatorHeartbeatTime;
    private final ElectionsService electionsService;


    public void assessCoordinatorHeartBeat(){
        var currentTime = System.currentTimeMillis();
        if (currentTime - lastCoordinatorHeartbeatTime > COORDINATOR_HEARTBEAT_TIMEOUT){
            log.error("COORDINATOR IS DOWN!");
            electionsService.startElectionIfNotExists();
        }
    }
    public void coordinatorHeartbeatReceived(){
        log.debug("Coordinator heartbeat received");
        lastCoordinatorHeartbeatTime = System.currentTimeMillis();
    }
}
