package edu.olehhaliak.mq.discoverability.follower;

import edu.olehhaliak.mq.config.BrokerConfig;
import edu.olehhaliak.mq.discoverability.coordinator.SubscribeRequest;
import edu.olehhaliak.mq.discoverability.election.CoordinatorUpdateAnnouncement;
import edu.olehhaliak.mq.discoverability.election.ElectionsService;
import edu.olehhaliak.mq.discoverability.registry.BrokerRegistry;
import edu.olehhaliak.mq.discoverability.registry.BrokerRegistryService;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
@RequiredArgsConstructor
@Slf4j
public class FollowerService {
    private final BrokerConfig brokerConfig;
    private final BrokerRegistryService brokerRegistryService;
    private final CoordinatorHeartbeatService coordinatorHeartbeatService;
    private final ElectionsService electionsService;
    private final RestTemplate restTemplate;


    @PostConstruct
    public void initTrigger() {
        if (!brokerConfig.isStandalone()) {
            init();
        }
    }

    private void init() {
        log.info("Reading bootstrap broker config");
        var bootstrapBrokerRegistry = getBootstrapBrokerRegistry();
        brokerRegistryService.updateRegistry(bootstrapBrokerRegistry);

        log.info("Subscribing to the coordinator");
        var coordinatorURL = bootstrapBrokerRegistry.getBrokers().get(bootstrapBrokerRegistry.getCoordinatorId());
        subscribeToCoordinator(coordinatorURL);

        coordinatorHeartbeatService.coordinatorHeartbeatReceived();
        log.info("Successfully subscribed to the coordinator");
    }

    public void newCoordinatorAnnounced(CoordinatorUpdateAnnouncement announcement){
        log.warn("New coordinator announced {}. Long live new coordinator!",announcement);
        brokerRegistryService.getRegistry().setCoordinatorId(announcement.getNewCoordinatorId());
        coordinatorHeartbeatService.coordinatorHeartbeatReceived();
        electionsService.electionsAreOver();
    }

    private  void subscribeToCoordinator(String coordinatorURL) {
        var url = coordinatorURL + "/cluster/coordinator/subscribe";
         restTemplate.postForEntity(url, buildSubscribeRequest(), Void.class);
    }


    private SubscribeRequest buildSubscribeRequest() {
        return SubscribeRequest.builder()
                .refererName(brokerConfig.getBrokerId())
                .url(brokerConfig.getUrl())
                .build();
    }

    private BrokerRegistry getBootstrapBrokerRegistry() {
        if(brokerConfig.getBootrapServerUrl().equals("none")){
            log.error("Broker is not in standalone mode and no bootstrap server was provided");
            throw new RuntimeException("Broker is not in standalone mode and no bootstrap server was provided");
        }
        var url = brokerConfig.getBootrapServerUrl() + "/cluster/registry";
        return restTemplate.getForObject(url, BrokerRegistry.class);
    }
}
