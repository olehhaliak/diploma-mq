package edu.olehhaliak.mq.metadata;

import edu.olehhaliak.mq.discoverability.coordinator.CoordinatorService;
import edu.olehhaliak.mq.topic.service.TopicService;
import edu.olehhaliak.mq.topology.PartitionDistributionService;
import edu.olehhaliak.mq.topology.TopologyClient;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
@RequiredArgsConstructor
@Slf4j
public class TopicMetadataService {
    private final TopicService topicService;
    private final PartitionDistributionService partitionDistributionService;
    private final CoordinatorService coordinatorService;
    private final TopologyClient topologyClient;

    public int getMetadataHash() {
        var topics = topicService.getTopics();
        var brokerDist = partitionDistributionService.getBrokerDistributionMap();
        return Objects.hash(topics, brokerDist);
    }


    public void propagateMetadata() {
        log.info("Propagating topic metadata");
        var followers = coordinatorService.getFollowers();
        var brokerDist = partitionDistributionService.getBrokerDistributionMap();
        followers.values().forEach(follower -> topologyClient.updateTopology(follower, brokerDist));
    }
}