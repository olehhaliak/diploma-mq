package edu.olehhaliak.mq.metadata;

import edu.olehhaliak.mq.discoverability.registry.BrokerRegistryService;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class TopicsMetadataUpdateTrigger {
    private final TopicMetadataService topicMetadataService;
    private final BrokerRegistryService brokerRegistryService;
    private volatile int lastHash;
    @PostConstruct
    public void init(){
        lastHash = topicMetadataService.getMetadataHash();
    }
    @Scheduled(fixedRate = 5_000)
    public void checkForUpdate(){
        int hash = topicMetadataService.getMetadataHash();
        if(brokerRegistryService.isCoordinator() && hash != lastHash){
            topicMetadataService.propagateMetadata();
            lastHash = hash;
        }
    }
}
