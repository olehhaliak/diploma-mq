FROM eclipse-temurin:21-jre-alpine

ADD target/mq-1.0.0.jar ~/
RUN apk add curl
WORKDIR ~/

ENV BROKER_HOSTNAME=localhost
ENV BOOTSTRAP_SERVER_URL=localhost
ENV STANDALONE_MODE=true

EXPOSE 9090

ENTRYPOINT ["java", "-jar", "mq-1.0.0.jar", "--spring.profiles.active=docker"]